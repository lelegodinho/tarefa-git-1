git help: Mostra vários comandos de forma resumida

git help <comando>: Mostra detalhes de um comando específico

git config --global user.name “Seu Nome”
git config --global user.email “seuemaill@gmail.com”   (Usar o mesmo email do gitlab)
 
Para o repositório atual: --local;
Para o Usuário: --global;
Para o Computador: --system

Opcional:
(primeiro testa code -v)
git config --global core.editor 'code --wait' -> Vscode para editor padrão (precisa do code no PATH)


	Se tudo der certo:
	git config --global -e -> Abre o arquivo de configuração no vscode

-------------------------------------------------------


git init: Inicia um repositório GIT em um determinado diretório

git status: Mostra a situação atual do repositório e de seus arquivos

git add arquivo/diretório: Adiciona o arquivo/diretório específico na área de preparo
git add --all = git add .: Adiona todos os arquivos criados ou modificados na área de preparo

git commit -m “Primeiro commit”: Salva no repositório local todas as modificações que estao armazenadas na área de preparo, junto com uma mensagem explicativa da atualização

-------------------------------------------------------

git log: Mostra o histórico de alterações em ordem cronológica de todos os commits realizados naquela branch
git log arquivo: Mostra o histórico apenas dos commits referentes ao arquivo espeficicado
git reflog: Resumo dos commits realizados

-------------------------------------------------------

git show: Exibe as mudanças realizadas no último commit de forma detalhada.
git show <commit>: Exibe as mudanças realizadas no commit espeficicado de forma detalhada.

-------------------------------------------------------

git diff: Faz uma comparação dos arquivos entre o que está na área de trabalho e commit mais recente
git diff <commit1> <commit2>: Faz uma comparação entre os dois commits especificados

-------------------------------------------------------

git reset --hard <commit>: Descarta todas as alterações na área de preparo além de reverter todas as alterações do diretório em treabalho para o estado do commit especificado

-------------------------------------------------------

git branch: Faz uma listagem de todas as branches locais
git branch -r: Faz uma listagem de todas as branches remotas 
git branch -a: Faz uma listagem de todas as branches locais e remotas
git branch -d <branch_name>: Faz a remoção de um branch local especificada
git branch -D <branch_name>: Força a remoção de um branch local especificada
git branch -m <nome_novo>: Renomeia a branch corrente
git branch -m <nome_antigo> <nome_novo>: Renomeia uma branch específica localmente que não é a corrente

-------------------------------------------------------

git checkout <branch_name>: Muda para a branch especificada que já seja existente
git checkout -b <branch_name>: Cria uma nova branch e já faz migra para a mesma 

-------------------------------------------------------

git merge <branch_name>: Mescla as mudanças presentes na branch especificada com a branch corrente

-------------------------------------------------------

git clone: Copia o repositório GIT existente para um diretório local 
git pull: Baixa o conteúdo do repositório remoto e atualiza o repositório local
git push: Envia as alterações realizadas no repositório local para o repositório remoto

-------------------------------------------------------

git remote -v: Faz uma lista com as URLs das conexões remotas com os outros repositórios
git remote add origin <url>: Adiciona uma URL de um repositório remoto para o envio dos commits do repositório local
git remote <url> origin: Aponta um repositorio remoto como origin

--------------------------------------------------------

Documentação do git:
https://git-scm.com/doc

Playlist GIT:
https://www.youtube.com/playlist?list=PLucm8g_ezqNq0dOgug6paAkH0AQSJPlIe

Vídeo sobre Git: 
https://www.youtube.com/watch?v=kB5e-gTAl_s


